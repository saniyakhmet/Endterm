//
//  TableViewCell.swift
//  Money
//
//  Created by mac on 21.02.2021.
//

import UIKit

class TableViewCell: UITableViewCell {

    var id = 0
    

    @IBOutlet weak var forLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
  
        }
    
}
