//
//  AddExpensesViewController.swift
//  Money
//
//  Created by mac on 21.02.2021.
//

import UIKit

var arr = [Calc]()
class AddExpensesViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
 
   
    
    @IBOutlet var forField: UITextField!
    @IBOutlet weak var amountField: UITextField!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    let cellId = "TableViewCell"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
     
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        forField.becomeFirstResponder()
        forField.delegate = self
        self.title = "Expenses"
        self.configureTableView()
    }
    
    func testDataConfigure(){
        arr.append(Calc(forF: "Food", amountE: "1000", dateE: "24.02.21"))
    
    }
    
    func configureTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableView.tableFooterView = UIView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        forField.resignFirstResponder()
        return true
    }
    
    @IBAction func addPressed(_ sender: UIButton) {
        if (forField.text != nil) && forField.text != "" && (amountField.text != nil) && amountField.text != "" && (dateField.text != nil) && dateField.text != "" {
            arr.append(Calc(forF: forField.text, amountE: amountField.text, dateE: dateField.text))
            forField.text = ""
            amountField.text = ""
            dateField.text = ""
            forField.placeholder = "Add a name"
            amountField.placeholder = "Add amount"
            dateField.placeholder = "Add a date"
        }
    }
    
    
}

extension AddExpensesViewController {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! TableViewCell
        let item = arr[indexPath.row]
        cell.forLabel.text = item.forF
        cell.amountLabel.text = item.amountE
        cell.dateLabel.text = item.dateE
        
        return cell
        
    }}

